module gitlab.com/gustavocd/service

go 1.15

require (
	github.com/ardanlabs/conf v1.3.3
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dimfeld/httptreemux/v5 v5.2.2
	github.com/dimiro1/darwin v0.0.0-20191008194338-370f81775d3b
	github.com/google/uuid v1.1.2
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.0.0
	github.com/pkg/errors v0.9.1
	go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp v0.15.1
	go.opentelemetry.io/otel v0.15.0
	go.opentelemetry.io/otel/exporters/trace/zipkin v0.15.0
	go.opentelemetry.io/otel/sdk v0.15.0
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
	google.golang.org/appengine v1.6.7 // indirect
)

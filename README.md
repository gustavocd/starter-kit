# Starter Kit

Starter Kit is a base project to develop production ready backend services in Go.

## Installation

```bash
git clone https://gitlab.com/gustavocd/starter-kit.git
```

## Usage

```bash
cd starter-kit
make run
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to 
discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://gitlab.com/gustavocd/starter-kit/-/blob/master/LICENSE)

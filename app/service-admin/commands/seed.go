package commands

import (
	"fmt"

	"github.com/pkg/errors"
	"gitlab.com/gustavocd/service/business/data/schema"
	"gitlab.com/gustavocd/service/foundation/database"
)

// Seed loads test data into the database.
func Seed(cfg database.Config) error {
	db, err := database.Open(cfg)
	if err != nil {
		return errors.Wrap(err, "connect database")
	}
	defer db.Close()

	if err := schema.Seed(db); err != nil {
		return errors.Wrap(err, "seed database")
	}

	fmt.Println("seed data complete")
	return nil
}
